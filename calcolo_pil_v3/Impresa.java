package calcolo_pil_v3;

/**
 *
 * @author MatteoDeRossi
 */
public class Impresa {
    
    int fatturato = 0;
    int fatt_prod_fin = 0;
    int salario = 0;
    int profitto = 0;
    int costi_int = 0;
    
    public Impresa(int fatt, int fatt_p_f, int sal, int pro, int cos_int){
        fatturato = fatt;
        fatt_prod_fin = fatt_p_f;
        salario = sal;
        profitto = pro;
        costi_int = cos_int;
    }
}
