package calcolo_pil_v3;

import java.util.*;

/**
 *
 * @author matteo.derossi
 */
public class Calcolo_pil_v3 {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int fatturato = 0;
        int fatt_prod_fin = 0;
        int salario = 0;
        int profitto = 0;
        int costi_int = 0;

        System.out.println("Quante aziende sono presenti nel calcolo?");
        System.out.println("Inserire numero di aziende");
        int nAziende = keyboard.nextInt();
        Impresa imprese[] = new Impresa [nAziende];
        
        for(int i = 0; i < nAziende; i++){
            System.out.println("A quanto ammontano i fatturati di azienda " + (i+1) + "?");
            fatturato = keyboard.nextInt();
            System.out.println("A quanto ammonta i fatturati dei prodotti finiti di azienda " + (i+1) + "?");
            fatt_prod_fin= keyboard.nextInt();
            System.out.println("A quanto ammontano i salari di azienda " + (i+1) + "?");
            salario = keyboard.nextInt();
            System.out.println("A quanto ammontano i profitti di azienda " + (i+1) + "?");
            profitto = keyboard.nextInt();
            System.out.println("A quanto ammontano i costi intermedi di azienda " + (i+1) + "?");
            costi_int = keyboard.nextInt();
            
            imprese[i] = new Impresa(fatturato, fatt_prod_fin, salario, profitto, costi_int);
        }
        
        System.out.println("Con quale metodo vuoi calcolare il PIL?");
        System.out.println("1: Metodo della spesa; 2: Metodo dei redditi; 3: Metodo del valore aggiunto");
        System.out.println("(Digitare 1, 2 o 3)");
        int metodoPIL = keyboard.nextInt();

        switch (metodoPIL) {
            case 1:
                metodoDellaSpesa(nAziende, imprese);
                break;
            case 2:
                metodoDeiRedditi(nAziende, imprese);
                break;
            case 3:
                metodoDelValoreAggiunto(nAziende, imprese);
                break;
            default:
                break;
        }

    }

    public static void metodoDellaSpesa(int nAz, Impresa [] imp) {
        int pil = 0;
        int sommaFatProdFin = 0;

        for (int j = 0; j < nAz; j++) {
            sommaFatProdFin += imp[j].fatt_prod_fin;
        }
        pil = sommaFatProdFin;
        
        System.out.println("Il PIL è di " + pil);
    }

    public static void metodoDeiRedditi(int nAz, Impresa [] imp) {
        int pil = 0;
        int sommaSalari = 0;
        int sommaProfitti = 0;

        for (int j = 0; j < nAz; j++) {
            sommaSalari += imp[j].salario;
            sommaProfitti += imp[j].profitto;
        }
        pil = sommaSalari + sommaProfitti;
        
        System.out.println("Il PIL è di " + pil);
    }

    public static void metodoDelValoreAggiunto(int nAz, Impresa [] imp) {
        int pil = 0;
        int sommaFatturati = 0;
        int sommaCostiInt = 0;

        for (int j = 0; j < nAz; j++) {
            sommaFatturati += imp[j].fatturato;
            sommaCostiInt += imp[j].costi_int;
        }
        pil = sommaFatturati - sommaCostiInt;
        
        System.out.println("Il PIL è di " + pil);
    }
}
