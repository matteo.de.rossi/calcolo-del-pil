Calcolo PIL

Studente: De Rossi Matteo

Classe: 5IB

- ! IMPORTANTE ! -

Scrivo questa anticipazione per avvertire che le mail (come Gmail) non permettono di inviare eseguibili (come file .exe e .jar). Netbeans, l'IDE con cui ho programmato, permette di creare solamente file .jar che non possono essere inviati via mail. Quindi mi trovo costretto a inviarle questo file di testo con questo link: 
" https://gitlab.com/matteo.de.rossi/calcolo-del-pil ". 
Il link serve a raggiungere un sito in cui ho salvato un file .zip con la cartella del progetto, il file eseguibile .jar, il file .java del programma e una versione testuale (.txt) del programma.

Per leggere il codice del file .java basta cliccare con il tasto destro sul file calcolo "CalcoloPIL.java", cliccare su "Apri con" e scegliere "Blocco note". Dovrei aver comunque mandato il file anche in versione testo (dovrebbe chiamarsi "CalcoloPIL.txt"). 

Per eseguire il file .jar bisogna utilizzare il "Prompt dei comandi". Attraverso il "Prompt dei comandi" bisogna entrare nella cartella in cui vi è il file "CalcoloPIL.jar" e digitare "Java -jar CalcoloPIL.jar" (questo è il procedimento su Windows). A questo punto il programma sarà in esecuzione.

- - 

Il programma esegue inizalmente il metodo "main".
Viene creato un oggetto di tipo "Scanner" per riuscire a leggere le risposte dell'utente da tastiera. Successivamente viene chiesto quale metodo di calcolo del PIL si vuole utilizzare. Dopo aver ricevuto la risposta, viene chiesta la quantità di aziende presenti della situazione da calcolare. Dopo aver ricevuto la risposta, grazie ad una condizione "switch" e "case" in cui comparo il metodo scelto dall'utente con i metodi disponibili, viene eseguito il metodo apposito del metodo scelto. 

Il metodo "metodoDellaSpesa" riceve dal metodo "main" l'oggetto della tastiera e il numero di aziende. Grazie ad un ciclo "for", ripeto per la quantintà di aziende presenti nel calcolo le seguenti azioni: chiedere a quanto ammontano i fatturati dei prodotti finiti dell'azienda, ricevere la risposta ed aggiungerla ad una variabile in cui vengono sommati tutti i fatturati dei prodotti finiti. Quando il ciclo finisce, viene mostrata all'utente la somma dei fatturati dei prodotti finiti, che rappresenta il PIL.

Il metodo "metodoDeiRedditi" riceve dal metodo "main" l'oggetto della tastiera e il numero di aziende. Grazie ad un ciclo "for", ripeto per la quantintà di aziende presenti nel calcolo le seguenti azioni: chiedere a quanto ammontano i salari dell'azienda, ricevere la risposta ed aggiungerla ad una variabile in cui vengono sommati tutti i salari; chiedere a quanto ammontano i profitti dell'azienda, ricevere la risposta ed aggiungerla ad una variabile in cui vengono sommati tutti i profitti. Quando il ciclo finisce, la somma dei salari e la somma dei profitti vengono sommate in una variabile che rappresenta il PIL, e viene mostrata all'utente tale variabile.

Il metodo "metodoDelValoreAggiunto" riceve dal metodo "main" l'oggetto della tastiera e il numero di aziende. Grazie ad un ciclo "for", ripeto per la quantintà di aziende presenti nel calcolo le seguenti azioni: chiedere a quanto ammontano i fatturati dell'azienda, ricevere la risposta ed aggiungerla ad una variabile in cui vengono sommati tutti i fatturati; chiedere a quanto ammontano i costi intermedi dell'azienda, ricevere la risposta ed aggiungerla ad una variabile in cui vengono sommati tutti i costi intermedi. Quando il ciclo finisce, viene calcolata la differenza tra la somma dei salari e la somma dei profitti, il calcolo viene inserito in una variabile che rappresenta il PIL, e viene mostrata all'utente tale variabile.