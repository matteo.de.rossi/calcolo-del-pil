package calcolopil;

import java.util.*;

/**
 *
 * @author matteo.derossi
 */
public class CalcoloPIL {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Con quale metodo vuoi calcolare il PIL?");
        System.out.println("1: Metodo della spesa; 2: Metodo dei redditi; 3: Metodo del valore aggiunto");
        System.out.println("(Digitare 1, 2 o 3)");
        int metodoPIL = keyboard.nextInt();
        
        System.out.println("Quante aziende sono presenti nel calcolo?");
        System.out.println("Inserire numero di aziende");
        int nAziende = keyboard.nextInt();

        switch (metodoPIL) {
            case 1:
                metodoDellaSpesa(keyboard, nAziende);
                break;
            case 2:
                metodoDeiRedditi(keyboard, nAziende);
                break;
            case 3:
                metodoDelValoreAggiunto(keyboard, nAziende);
                break;
            default:
                break;
        }

    }

    public static void metodoDellaSpesa(Scanner kb, int nAz) {
        int pil = 0;
        int sommaFatProdFin = 0;

        for (int i = 0; i < nAz; i++) {
            System.out.println("A quanto ammonta i fatturati dei prodotti finiti di azienda " + (i+1) + "?");
            sommaFatProdFin += kb.nextInt();
        }
        pil = sommaFatProdFin;
        
        System.out.println("Il PIL è di " + pil);
    }

    public static void metodoDeiRedditi(Scanner kb, int nAz) {
        int pil = 0;
        int sommaSalari = 0;
        int sommaProfitti = 0;

        for (int i = 0; i < nAz; i++) {
            System.out.println("A quanto ammontano i salari di azienda " + (i+1) + "?");
            sommaSalari += kb.nextInt();
            System.out.println("A quanto ammontano i profitti di azienda " + (i+1) + "?");
            sommaProfitti += kb.nextInt();
        }
        pil = sommaSalari + sommaProfitti;
        
        System.out.println("Il PIL è di " + pil);
    }

    public static void metodoDelValoreAggiunto(Scanner kb, int nAz) {
        int pil = 0;
        int sommaFatturati = 0;
        int sommaCostiInt = 0;

        for (int i = 0; i < nAz; i++) {
            System.out.println("A quanto ammontano i fatturati di azienda " + (i+1) + "?");
            sommaFatturati += kb.nextInt();
            System.out.println("A quanto ammontano i costi intermedi di azienda " + (i+1) + "?");
            sommaCostiInt += kb.nextInt();
        }
        pil = sommaFatturati - sommaCostiInt;
        
        System.out.println("Il PIL è di " + pil);
    }
}
